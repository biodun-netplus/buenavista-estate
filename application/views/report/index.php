<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')): ?>
            <span class="pull-right" style="padding: 10px;">Meter No :
                <?= $this->aauth->get_user()->meter_no ?>
  </span>
        <?php endif; ?>
        <div class="page-heading">
            <h1>Reports</h1>

            <div class="options"></div>
        </div>
        <div class="container-fluid">
            <?php $this->load->view('includes/notification'); ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Payment Report List</h2>

                                <div class="panel-ctrls"></div>
                            </div>

                            <div class="panel-body">
                                <?php echo form_open('report/export_payment_report', ['id' => "validate-form", "class" => "form-horizontal"]); ?>

                                <div class="col-xs-12">
                                    <label>Payment Report Date Filter</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input name="payment_report_start_date" class="date-calendar form-control" placeholder="Start date"/>
                                        </div>
                                        <div class="col-md-4">
                                            <input name="payment_report_end_date"  class="date-calendar form-control" placeholder="End date"/>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary btn-raised pull-right">Export</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>


                        </div>
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Individual User Report List</h2>

                                <div class="panel-ctrls"></div>
                            </div>

                            <div class="panel-body">
                                <?php echo form_open('report/export_user_report', ['id' => "validate-form", "class" => "form-horizontal"]); ?>

                                <div class="col-xs-4">
                                    <label>User</label>
                                    <select class="form-control" id="select1" name="user" required>
                                        <?php foreach ($users as $user): ?>
                                            <option value="<?= $user->id ?>"><?= $user->full_name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-xs-8">
                                    <label>Date</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input name="user_report_start_date" class="date-calendar form-control" placeholder="Start date"/>
                                        </div>
                                        <div class="col-md-4">
                                            <input name="user_report_end_date" class="date-calendar form-control" placeholder="End date"/>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary btn-raised pull-right">Export</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>


</body>