<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>

        <div class="page-heading">
            <h1>Create Outstanding Bill</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">

            <?php $this->load->view('includes/notification'); ?>
            <div class="col-sm-6" style="float:none;margin:auto;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Create Outstanding Payment</h2>
                    </div>
                    <?php echo form_open('outstanding/create', ['id' => "validate-form", "class" => "form-horizontal"]); ?>

                    <div class="panel-body">
                        <div class="form-group mb-md">
                            <div class="col-xs-12">
                                <label>Outstanding Bill Title</label>
                                <input type="text" class="form-control" name="description" id="description"
                                       placeholder="" required>
                            </div>
                        </div>
                    <div class="form-group mb-md">
                        <div class="col-xs-12">
                            <label>Outstanding Amount</label>
                            <input type="text" class="form-control" name="outstanding_amount" id="outstanding_amount"
                                   placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group mb-md">

                        <div class="col-xs-12">
                            <label>Assign User</label>
                            <select class="form-control" id="select1" name="user" required>
                                <?php foreach($users as $user): ?>
                                    <option value="<?= $user->id ?>"><?= $user->full_name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    <div class="clearfix">
                        <button type="submit" name="create_o_bill" class="btn btn-primary btn-raised pull-right">Create Outstanding Bill</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- .container-fluid -->
</div>
<!-- #page-content -->
</div>
