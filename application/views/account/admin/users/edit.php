
            <div class="main">
                <div class="breadcrumb">
                    <a href="#">Administrator</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo site_url('admin/users') ?>">User</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo site_url('admin/user/new/'.$user->id) ?>">Edit</a>
                </div>
                <div class="content">
                    <div class="panel">
                        <div class="content-header no-mg-top">
                            <i class="fa fa-newspaper-o"></i>
                            <div class="content-header-title">Edit Administrator</div>
                        </div>
                        
                        <div class="row" >
                            <div class="col-md-8" style="float:none;margin:auto;">
                                <?php $this->load->view('includes/notification'); ?>
									
                                <div class="content-box">
                                    <div class="content-box-header">
										<div class="box-title">Update Administrator</div>
									</div>
                                    <?php
                                        $attributes = array( 'id' => 'form-validate');
                                        echo form_open('admin/user/update/'.$user->id, $attributes);
                                    ?>
									    <div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label>User Name</label>
													<input class="form-control" data-error="Please input username" placeholder="Enter Username" required="required" type="text" name="username" value="<?php echo $user->username ?>">
													<div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Email address</label>
                                                    <input class="form-control" data-error="Email address is invalid" placeholder="Enter Email" required="required" type="email"  name="email" value="<?php echo $user->email ?>" disabled>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                              
											<div class="col-sm-6">
												<div class="form-group">
                                                    <label>Password</label>
                                                    <input class="form-control"  placeholder="Change Password" type="password"  name="password">
                                                 
												</div>
											</div>
                                            
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Account Status</label>
                                                    <select name="status" class="form-control" required="required" >
                                                        <option></option>
                                                        <option value="0" <?php echo ($user->banned == 0) ? 'selected': '' ; ?>>Active</option>
                                                        <option value="1" <?php echo ($user->banned == 1) ? 'selected': '' ; ?>>De-activated</option>
                                                    </select>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                          
                                            
										</div>
                                     
                                        
                                        
										<div class="content-box-footer">
											<button class="btn btn-primary" type="submit"><i class="fa fa-pencil"></i> Update</button>
										</div>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>