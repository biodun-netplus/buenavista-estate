
            <div class="main">
                <div class="breadcrumb">
                    <a href="#">Administrator</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo site_url('admin/merchants') ?>">Merchant</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo current_url() ?>">Edit</a>
                </div>
                <div class="content">
                    <div class="panel">
                        <div class="content-header no-mg-top">
                            <i class="fa fa-newspaper-o"></i>
                            <div class="content-header-title">Edit Merchant</div>
                        </div>
                        
                        <div class="row" >
                            <div class="col-md-8" style="float:none;margin:auto;">
                                <?php $this->load->view('includes/notification'); ?>
								
                                <div class="content-box">
									<div class="content-box-header">
										<div class="box-title">Update Merchant Account</div>
									</div>
									<?php
                                        $attributes = array( 'id' => 'form-validate');
                                        echo form_open('admin/merchant/update/'.$merchant->merchant_id, $attributes);
                                    ?>    <div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label>Company Name</label>
													<input class="form-control" data-error="Please input conpany name" placeholder="Enter Company Name" required="required" type="text"  name="company_name" value="<?php echo $merchant->company_name ?>">
													<div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Net Plus Merchant ID</label>
													<input class="form-control" data-error="Please input Net Plus Merchant ID" placeholder="Enter Net Plus Merchant ID" required="required" type="text"  name="merchant_id" value="<?php echo $merchant->net_plus_merchant_id ?>">
													<div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Email address</label>
                                                    <input class="form-control" data-error="Email address is invalid" placeholder="Enter Email" required="required" type="email"  name="email" value="<?php echo $merchant->email ?>">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Contact Number</label>
													<input class="form-control" data-error="Please input Contact Number" placeholder="Enter Contact Number" required="required" type="text"  name="contact_no"  value="<?php echo $merchant->phone ?>">
													<div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
                                                    <label>Password</label>
                                                    <input class="form-control" id="inputPassword" placeholder="Enter Password" type="password"  name="password">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Account Status</label>
                                                    <select name="status" class="form-control" required="required" >
                                                        <option></option>
                                                        <option value="0" <?php echo ($merchant->banned == 0) ? 'selected': '' ; ?>>Active</option>
                                                        <option value="1" <?php echo ($merchant->banned == 1) ? 'selected': '' ; ?>>De-activated</option>
                                                    </select>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Contact Address</label>
													<textarea class="form-control" rows="5" data-error="Please enter Contact Address" required="required"  name="address"> <?php echo $merchant->contact_address ?></textarea>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label>Enable Saddle Integration</label>
                                                    <select id="saddle_int" name="saddle_int" class="form-control" required="required" >
                                                        <option></option>
                                                        <option value="1" <?php echo ($merchant->saddle_int == 1) ? 'selected': '' ; ?>>Yes</option>
                                                        <option value="0" <?php echo ($merchant->saddle_int == 0) ? 'selected': '' ; ?>>No</option>
                                                    </select>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>

										</div>
                                        
										<div id="saddle" class="row">
                                            
                                        </div>
                                        
                                        
										<div class="content-box-footer">
											<button class="btn btn-primary" type="submit"><i class="fa fa-edit"></i> Update</button>
										</div>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


			
<script type="application/javascript">
    $(document).ready(function(){
		saddle_info();
       	$("#saddle_int").change(function(){
         	saddle_info(); 
        });
 
    });

	function saddle_info(){
		 var selected = $("#saddle_int").val();
           //alert(selected);
           if(selected == 1){
            $.post("<?php echo site_url('admin/form/component/saddle?merchant_id='.$merchant->id); ?>", function(data, status){
                
                $('#saddle').html(data);
                $('#form-validate').validator('update');
                //alert("Data: " + data + "\nStatus: " + status);
            });
           }else{
               $('#saddle').html('');
               $('#form-validate').validator('update');
           }
           
	}
</script>