<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
       
        <div class="page-heading">
            <h1>Vend Power</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">

        <?php $this->load->view('includes/notification'); ?>
            <div class="col-sm-6" style="float:none;margin:auto;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Create Vend Power</h2>
                    </div>
                  
                    <?php echo form_open('power/saveTransaction', ['id' => "validate-form"]); ?>
                    <div class="panel-body">
                        <div class="form-group mb-md">
                             
                            <div class="col-xs-8">
                                <label>Assign User</label>
                                <select class="form-control" id="select1" name="user" required>
                                <option value="">Select Name</option>
                                    <?php foreach($users as $user): ?>
                                    <option value="<?= $user->id ?>"><?= $user->full_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            
                        </div>

                        <div class="col-xs-4">
                            <input type="button" value="Verify" id="verify" class="btn btn-raised btn-success pull-right"/>
                            </div>

                        <div class="form-group mb-md">                            
                            <div class="col-xs-12">
                                <label>Amount</label>
                                <input type="text" class="form-control" name="power_amount" id="power_amount"
                                       placeholder="" required>
                            </div>
                        </div>

                        <div class="form-group mb-md">                            
                            <div class="col-xs-12">
                                <label>Charges</label>
                                <input type="text" class="form-control" name="charge" id="charge" value="0"
                                       placeholder="" required>
                            </div>
                        </div>

                        <input type="hidden" name="order_id" id="order_id" value="<?php echo  $order_id; ?>" />
                        
     
                    <div class="panel-footer">
                        <div class="clearfix">
                            <button type="submit" name="vend_power" id="vend_power" class="btn btn-primary btn-raised pull-right">Vend Power</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->   
    </div>
    <!-- #page-content -->
</div>


<script style="text/javascript">
    $(document).ready(function () {
        $('#vend_power').hide();
        $('#verify').on('click', function (e) {
		    e.preventDefault();
            var user = $('#select1').val();
            var charge = $('#charge').val();
            var amount = $('#power_amount').val();
            $.ajax({
                type: "GET",
                url: "/power/verify_meter",
                data: {"user": user, "charge": charge, "amount":amount},
                success: function(data) {
                   
                  var value = JSON.parse(data);
                  console.log(value);
                  if(value.code == 00){
                        $('#vend_power').show();
                  }else{
                        $('#vend_power').hide();
                        window.location = '<?php echo base_url('power/create') ?>';
                  }
                
                }
            });

        });


        $("#vend_power").click(function (e) {
            e.preventDefault();
            var user = $('#select1').val();
            var charge = $('#charge').val();
            var amount = $('#power_amount').val();
            var order_id = $('#order_id').val();
            // Ajax call to initailize transaction
            $.ajax({
                type: "POST",
                url: "/power/saveTransaction",
                data: {"user": user, "charge": charge, "amount":amount, "order_id": order_id},
                success: function(data) {
                   console.log(data)
                   var value = JSON.parse(data);
                    if(value.code == 00){
                        window.location = '<?php echo base_url('power/payment_response') ?>';
                    }else{
                        window.location = '<?php echo base_url('power/create') ?>';
                    } 
                }
                
            });

        });
    });


</script>