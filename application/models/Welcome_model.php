<?php

class Welcome_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

	}
	public function save($data)
	{
		$this->db->insert('orders', $data);
		$lastId = $this->db->insert_id();
		return $lastId;

	}
	public function get($id)
	{
		$this->db->select('*');
		$query = $this->db->get_where('orders', array('id' => $id));
		return $query->row_array();
	}
}