-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2019 at 09:28 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buenavisate_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `aauth_groups`
--

CREATE TABLE `aauth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_groups`
--

INSERT INTO `aauth_groups` (`id`, `name`, `definition`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Public', 'Public Access Group'),
(4, 'Merchant', 'Merchant Accounts'),
(5, 'Finance', 'Finance User Group'),
(6, 'Report', 'Report User Group');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_group_to_group`
--

CREATE TABLE `aauth_group_to_group` (
  `group_id` int(11) UNSIGNED NOT NULL,
  `subgroup_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_login_attempts`
--

CREATE TABLE `aauth_login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(39) DEFAULT '0',
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aauth_login_attempts`
--

INSERT INTO `aauth_login_attempts` (`id`, `ip_address`, `timestamp`, `login_attempts`) VALUES
(11, '127.0.0.1', '2018-12-24 17:46:00', 2),
(14, '127.0.0.1', '2018-12-29 12:43:06', 3),
(15, '127.0.0.1', '2018-12-29 12:59:40', 3),
(20, '127.0.0.1', '2018-12-29 21:23:12', 1),
(27, '127.0.0.1', '2019-01-09 14:59:13', 5);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perms`
--

CREATE TABLE `aauth_perms` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_group`
--

CREATE TABLE `aauth_perm_to_group` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_user`
--

CREATE TABLE `aauth_perm_to_user` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_pms`
--

CREATE TABLE `aauth_pms` (
  `id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(1) DEFAULT NULL,
  `pm_deleted_receiver` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_users`
--

CREATE TABLE `aauth_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `cug_no` varchar(255) DEFAULT NULL,
  `meter_no` varchar(255) DEFAULT NULL,
  `house_address` varchar(255) NOT NULL,
  `type_of_property` varchar(255) NOT NULL,
  `type_of_ownership` varchar(50) DEFAULT NULL,
  `partner_type` varchar(255) NOT NULL,
  `banned` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `login_count` int(11) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `totp_secret` varchar(16) DEFAULT NULL,
  `ip_address` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_users`
--

INSERT INTO `aauth_users` (`id`, `email`, `pass`, `username`, `full_name`, `mobile_no`, `cug_no`, `meter_no`, `house_address`, `type_of_property`, `type_of_ownership`, `partner_type`, `banned`, `last_login`, `last_activity`, `login_count`, `date_created`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `totp_secret`, `ip_address`) VALUES
(2, 'admin@admin.com', '4f6951746d1b7d120d7e7c3d6fbede71139b38940601845bee81926a73ef4eaf', '', 'testq lsd', '0937434756', '', '623170120867', '34 Ilamoye', 'Semi Detached', 'landlord', 'Partner', 0, '2019-01-16 15:55:23', '2019-01-16 15:55:23', 32, '2018-12-29 15:25:11', NULL, NULL, NULL, NULL, NULL, '127.0.0.1'),
(3, 'manassehl9@gmail.com', 'f02af0215a09369087b091fe6aefedd5d077732cedb1cbc053f798b8aa61b604', '', 'test', '90989987', '', '233458436', 'B10 1004', 'Semi Detached', 'landlord', 'Partner', 0, '2018-12-29 15:31:03', '2018-12-29 15:31:03', 1, '2018-12-29 15:29:11', NULL, NULL, NULL, NULL, NULL, '127.0.0.1'),
(12, 'manie@gmail.com', '8fca0600d6c74c2d3e1ae2d43cd1bd5a2bad9ea9a4898f2ad4f5d37aa11c6053', '', 'john boo', '0938294896', '', '253463689', 'a7', 'Detached', 'resident', 'Partner', 0, '2019-01-10 12:14:23', '2019-01-10 12:14:24', 7, '2019-01-02 13:09:09', NULL, NULL, NULL, NULL, NULL, '127.0.0.1'),
(21, 'maniea@gmail.com', 'f5ea8584e42d3512ff9fd68d2c919372530108dad0fe016c1454fd1d1d638ca4', '', 'Manasseh', '09087665', '', '292345742', '61 E University Ave', 'Detached', 'landlord', 'Partner', 0, '2019-01-16 15:10:19', '2019-01-16 15:10:22', 8, '2019-01-10 12:17:39', NULL, NULL, NULL, NULL, NULL, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_to_group`
--

CREATE TABLE `aauth_user_to_group` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_user_to_group`
--

INSERT INTO `aauth_user_to_group` (`user_id`, `group_id`) VALUES
(2, 1),
(3, 2),
(12, 2),
(13, 2),
(15, 2),
(18, 2),
(19, 2),
(21, 2);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_variables`
--

CREATE TABLE `aauth_user_variables` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `tittle` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `cart_session` varchar(255) NOT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `meter_no` varchar(255) NOT NULL,
  `product_price_type` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `product_type` varchar(255) NOT NULL DEFAULT 'PRODUCT',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `product_id`, `product_name`, `price`, `quantity`, `cart_session`, `order_id`, `meter_no`, `product_price_type`, `payment_status`, `product_type`, `date`) VALUES
(1, '18', 2, 'December Service', '1000', '1', '5c3650a3ee8e862523f858dc2c9e129e4b0bc3a04e630ad9f3a7f', NULL, '43645747', '', 'Pending', 'PRODUCT', '2019-01-09'),
(2, '18', 5, 'December Service', '1000', '1', '5c3652062caf7adcd31ba5f797e560bc9592cb73e0cb18c69d538', '55300281764', '43645747', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(3, '18', 5, 'December Service', '4000', '4', '5c365214a5444adcd31ba5f797e560bc9592cb73e0cb18c69d538', '55026389471', '43645747', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(4, '18', 3, 'December Water Bill', '3000', '1', '5c36524d2146dadcd31ba5f797e560bc9592cb73e0cb18c69d538', '55926804371', '43645747', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(5, '19', 6, 'December Service', '2000', '1', '5c36600fc1cac1649673fd2fa316320596385efe9b41cedb91a58', '55821743096', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(6, '19', 6, 'December Service', '12000', '6', '5c36601e93fc61649673fd2fa316320596385efe9b41cedb91a58', '55804129763', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(7, '19', 6, 'December Service', '2000', '1', '5c3661bbd07190044f3c717c717d35c9feff63fee765fef5d9d12', '55604387912', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(8, '19', 6, 'December Service', '2000', '1', '5c3664ba3888eaaa1028c3cd26f59cb15484879588cb63c3a0ea0', NULL, '2534636', '', 'Pending', 'PRODUCT', '2019-01-09'),
(9, '19', 6, 'December Service', '4000', '1', '5c3664eae59043c496b2021a5b3f6cb7825c6d0494781f4fc4397', '55847123009', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(10, '19', 6, 'December Service', '2000', '1', '5c3666836e48fe5324f9a1856442520482b984ad80162d78b9035', NULL, '2534636', '', 'Pending', 'PRODUCT', '2019-01-09'),
(11, '19', 6, 'December Service', '2000', '1', '5c3666896cf15d5002d7c2a7591d201c16cd542b38e31c540c9b6', '55093461728', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(12, '19', 8, 'December Security Charge', '1500', '1', '5c3666b384ca1e5324f9a1856442520482b984ad80162d78b9035', '55348009761', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(13, '19', 7, 'December Water Bill', '2000', '1', '5c3666e8b44fbd5002d7c2a7591d201c16cd542b38e31c540c9b6', '55107869243', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(14, '19', 6, 'December Service', '2000', '1', '5c3669389d12e288c5e68df95356246d4546d9fb915e0106254d7', '55243796180', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(15, '19', 6, 'December Service', '2000', '1', '5c366a704aa20b8eb596a10d4cfb05bad10c3076229185a1731d3', '55016234897', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(16, '19', 6, 'December Service', '2000', '1', '5c366c4cde07b2385313ecc0aba45d63ccd0b32ad862fb90783fb', '55674029831', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-09'),
(17, '19', 6, 'December Service', '2000', '1', '5c3707feae659714d8594bcb26960d3791bbdb2126e9eca36f76e', '55243769018', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-10'),
(18, '19', 6, 'December Service', '2000', '1', '5c370850e68cd714d8594bcb26960d3791bbdb2126e9eca36f76e', '55109743826', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-10'),
(19, '19', 6, 'December Service', '4000', '2', '5c37085e96911714d8594bcb26960d3791bbdb2126e9eca36f76e', '55821769003', '2534636', 'Total', 'Pending', 'PRODUCT', '2019-01-10'),
(20, '21', 6, 'December Service', '1', '1', '5c3c862f5311c34fb994d9c74f4108b98f04155a96402fa95c9a1', '55612009734', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(21, '21', 6, 'December Service', '1', '1', '5c3c86fe53cb11a1fb624a3b1bfd987eb995bb9c2fbdd5358e91d', '55703296148', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(22, '21', 6, 'December Service', '1', '1', '5c3c877e952431a1fb624a3b1bfd987eb995bb9c2fbdd5358e91d', '55871926430', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(23, '21', 6, 'December Service', '1', '1', '5c3c8c1ed231b516ea4e171a8d2c33ca3871671d07169af693f77', '55608927431', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(24, '21', 6, 'December Service', '1', '1', '5c3c8cf8aceeb516ea4e171a8d2c33ca3871671d07169af693f77', '55184620073', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(25, '21', 6, 'December Service', '10', '1', '5c3c8e820bc055afc95b47ebcbe63b5f1e7d5e2f2935fd5bf7cc9', '55130768924', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(26, '21', 6, 'December Service', '10', '1', '5c3c9a9a93fe4d3084354ccc3fc16eb30ba4a1ab69b1bc78c1c4d', '55781096432', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(27, '21', 6, 'December Service', '10', '1', '5c3c9c12e2b61b2db3aed216bd24478d2a6f0cdd3c53d4e7b9940', '55412730869', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(28, '21', 6, 'December Service', '10', '1', '5c3ca6aeb5dd353e79a16decd4390b229b76b2fafc38a87618ed4', '55394276810', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(29, '21', 6, 'December Service', '10', '1', '5c3caffbf1f91553e956e1b8b44c8f628a5afe460e5d443267bc1', '55100892436', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(30, '21', 6, 'December Service', '10', '1', '5c3cb11c77613e5d79727938568944a5063c502d4a07aa10157ce', '55163087429', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(31, '21', 6, 'December Service', '10', '1', '5c3cb32851d4281712cfb6ec4eef34cbde971cc24ac2a14beb89d', '55412960087', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(32, '21', 6, 'December Service', '10', '1', '5c3cb352495d981712cfb6ec4eef34cbde971cc24ac2a14beb89d', '55760913842', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(33, '21', 6, 'December Service', '10', '1', '5c3cea0cc2e2ea760c48611290b7e4dfe244f52635c058f2190f6', '55341972800', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(34, '21', 6, 'December Service', '5', '1', '5c3cec594c5fe837c83c54638c627ab87a86b9b9231a811f4fee2', '55196072438', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(35, '21', 6, 'December Service', '10', '1', '5c3ced66f1b0eccddd9ab885350aa83440dd6ac76b91e67977a1e', '55736421009', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(36, '21', 6, 'December Service', '10', '1', '5c3cefa6c4dd4b53a528b953358db17adf9f8fe6c534b5d047af7', '55006971483', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(37, '21', 6, 'December Service', '10', '1', '5c3cf2644d652924d9eb118e12d99a7a84feaf2946c4bc17ea88f', '55746209813', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(38, '21', 6, 'December Service', '10', '1', '5c3cf52d1b945b8d114f1527d1b972c67432dd392b670f1e5e6d4', '55237490681', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(39, '21', 6, 'December Service', '10', '1', '5c3cf7b40c5cf1f33d07eb66fda6e6e633fb74cbf8f0c79421a76', '55729461083', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(40, '21', 6, 'December Service', '10', '1', '5c3cf87548c561f33d07eb66fda6e6e633fb74cbf8f0c79421a76', '55649130287', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-14'),
(41, '21', 3, 'Outstanding', '123', '1', '5c3f3b59215f42db5bcff7e6564fb1f9115089cca10f9e579a323', '55240798136', '292345742', 'Total', 'Pending', 'BILL', '2019-01-16'),
(42, '21', 4, 'test', '30', '1', '5c3f3c2149bbdbb03d043d096c59a06798f30536cf25900b430ce', '55614309872', '292345742', 'Total', 'Pending', 'BILL', '2019-01-16'),
(43, '21', 4, 'test', '30', '1', '5c3f405788d5fae63d761b7c26d050206abfbe1b83403e92bda0f', '55261908734', '292345742', 'Total', 'Pending', 'BILL', '2019-01-16'),
(44, '21', 5, 'manie', '890', '1', '5c3f41bdba2d60feb7ddbf4a6fe5f7b435740e437faceae158d6b', '55413002697', '292345742', 'Total', 'Pending', 'BILL', '2019-01-16'),
(45, '21', 5, 'manie', '890', '1', '5c3f431a8c5dbe2d281370cc9b8adffac237e88aace3db3f171c4', NULL, '292345742', '', 'Pending', 'BILL', '2019-01-16'),
(46, '21', 5, 'manie', '890', '1', '5c3f4336c49efe2d281370cc9b8adffac237e88aace3db3f171c4', NULL, '292345742', '', 'Pending', 'BILL', '2019-01-16'),
(47, '21', 5, 'manie', '890', '1', '5c3f445c0e07da3494784b2b65f1da8abe9f4c38fa3bd75c60f81', NULL, '292345742', '', 'Pending', 'BILL', '2019-01-16'),
(48, '21', 5, 'manie', '890', '1', '5c3f4490a1196a3494784b2b65f1da8abe9f4c38fa3bd75c60f81', NULL, '292345742', '', 'Pending', 'BILL', '2019-01-16'),
(49, '21', 5, 'manie', '890', '1', '5c3f44ab65444a3494784b2b65f1da8abe9f4c38fa3bd75c60f81', '55132964780', '292345742', 'Total', 'Pending', 'BILL', '2019-01-16'),
(50, '21', 5, 'manie', '890', '1', '5c3f45f203fa67de2017eb6c6146988e78022e1dbc02e3cc0c672', '55978264100', '292345742', 'Total', 'Pending', 'BILL', '2019-01-16'),
(51, '21', 7, 'December Water Bill', '2000', '1', '5c3f47b95b576b7be1fa5a0559b6c40e373c5ec761ca811f07d64', '55143608279', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-16'),
(52, '21', 7, 'December Water Bill', '2000', '1', '5c3f485866a48b7be1fa5a0559b6c40e373c5ec761ca811f07d64', '55831240076', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-16'),
(53, '21', 6, 'Outstanding', '10', '1', '5c3f48957c993b7be1fa5a0559b6c40e373c5ec761ca811f07d64', '55937860214', '292345742', 'Total', 'Pending', 'BILL', '2019-01-16'),
(54, '21', 7, 'December Water Bill', '2000', '1', '5c3f4b82dbf165896e0291cdc8da8f717f3afcaf852fdf9992607', '55823149600', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-16'),
(55, '21', 7, 'December Water Bill', '2000', '1', '5c3f4b9457ee05896e0291cdc8da8f717f3afcaf852fdf9992607', '55413289607', '292345742', 'Total', 'Pending', 'PRODUCT', '2019-01-16');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `coupon_value` double NOT NULL,
  `total_usage` int(11) NOT NULL,
  `usage_count` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `house_address`
--

CREATE TABLE `house_address` (
  `id` int(11) NOT NULL,
  `house_number` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `house_address`
--

INSERT INTO `house_address` (`id`, `house_number`) VALUES
(1, 'A4'),
(2, 'A5'),
(3, 'A6'),
(4, 'A7'),
(5, 'A8'),
(6, 'A9'),
(7, 'A10'),
(8, 'A11'),
(9, 'A12'),
(10, 'A13'),
(11, 'A19'),
(12, 'A20'),
(13, 'A21'),
(14, 'A22'),
(15, 'A23'),
(16, 'A24'),
(17, 'B5'),
(18, 'B6'),
(19, 'B7'),
(20, 'B8'),
(21, 'B9'),
(22, 'B10'),
(23, 'B11'),
(24, 'B12'),
(25, 'B13'),
(26, 'B14'),
(27, 'B15'),
(28, 'B16'),
(29, 'B17'),
(30, 'B18'),
(31, 'B19'),
(32, 'B20'),
(33, 'B21'),
(34, 'B22'),
(35, 'B23'),
(36, 'B1A'),
(37, 'B1B'),
(38, 'B1C'),
(39, 'B2A'),
(40, 'B2B'),
(41, 'B2C'),
(42, 'C5'),
(43, 'C6'),
(44, 'C7'),
(45, 'C8'),
(46, 'C9'),
(47, 'C10'),
(48, 'C11'),
(49, 'C12'),
(50, 'C13'),
(51, 'C14'),
(52, 'C15'),
(53, 'C16'),
(54, 'C17'),
(55, 'C18'),
(56, 'C19'),
(57, 'C20'),
(58, 'D5'),
(59, 'D6'),
(60, 'D7'),
(61, 'D8'),
(62, 'D9'),
(63, 'D10'),
(64, 'D11'),
(65, 'D12'),
(66, 'D13'),
(67, 'D14'),
(68, 'D15'),
(69, 'D16'),
(70, 'D17'),
(71, 'D18'),
(72, 'D19'),
(73, 'D20'),
(74, 'E5'),
(75, 'E8'),
(76, 'E9'),
(77, 'F7'),
(78, 'F8'),
(79, 'F9'),
(80, 'F10'),
(81, 'F11'),
(82, 'F12'),
(83, 'F13'),
(84, 'F14'),
(85, 'F15'),
(86, 'F16'),
(87, 'G5'),
(88, 'G6'),
(89, 'G7'),
(90, 'G8'),
(91, 'G9'),
(92, 'H1'),
(93, 'H2'),
(94, 'H3'),
(95, 'H4'),
(96, 'H5'),
(97, 'H6'),
(98, 'H7'),
(99, 'H8'),
(100, 'H9'),
(101, 'H10'),
(102, 'H11'),
(103, 'H12'),
(104, 'H13'),
(105, 'H14'),
(106, 'H15'),
(107, 'H16'),
(108, 'H17'),
(109, 'H18'),
(110, 'H19'),
(111, 'H20'),
(112, 'H21'),
(113, 'H22'),
(114, 'H23'),
(115, 'H24'),
(116, 'H25'),
(117, 'H26'),
(118, 'H27'),
(119, 'H28'),
(120, 'H29'),
(121, 'I1A'),
(122, 'I1B'),
(123, 'I1C'),
(124, 'I1D'),
(125, 'I2A'),
(126, 'I2B'),
(127, 'I2C'),
(128, 'I2D'),
(129, 'I4A'),
(130, 'I4B'),
(131, 'I4C'),
(132, 'I4D'),
(133, 'I5A'),
(134, 'I5B'),
(135, 'I5C'),
(136, 'I5D'),
(137, 'I6A'),
(138, 'I6B'),
(139, 'I6C'),
(140, 'I6D'),
(141, 'I7A'),
(142, 'I7B'),
(143, 'I8A'),
(144, 'I8B'),
(145, 'I8C'),
(146, 'I8D'),
(147, 'I9A'),
(148, 'I9B'),
(149, 'I9C'),
(150, 'I9D'),
(151, 'I10A'),
(152, 'I10B'),
(153, 'I10C'),
(154, 'I10D'),
(155, 'I11A'),
(156, 'I11B'),
(157, 'I11C'),
(158, 'I11D'),
(159, 'I11E'),
(160, 'I11F'),
(161, 'I12A'),
(162, 'I12B'),
(163, 'I12C'),
(164, 'I12D'),
(165, 'I12E'),
(166, 'I12F'),
(167, 'I13A'),
(168, 'I13B'),
(169, 'I13C'),
(170, 'I13D'),
(171, 'I13E'),
(172, 'I13F'),
(173, 'I15A'),
(174, 'I15B'),
(175, 'I15C'),
(176, 'I15D'),
(177, 'I16A'),
(178, 'I16B'),
(179, 'I16C'),
(180, 'I16D'),
(181, 'I17A'),
(182, 'I17B'),
(183, 'I17C'),
(184, 'I17D'),
(185, 'I19A'),
(186, 'I19B'),
(187, 'I19C'),
(188, 'I19D'),
(189, 'I20A'),
(190, 'I20B'),
(191, 'I20C'),
(192, 'I20D'),
(193, 'I21A'),
(194, 'I21B'),
(195, 'I21C'),
(196, 'I21D');

-- --------------------------------------------------------

--
-- Table structure for table `meter_tariff`
--

CREATE TABLE `meter_tariff` (
  `id` int(11) NOT NULL,
  `tariff_name` varchar(255) NOT NULL,
  `tariff_price` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outstanding_bills`
--

CREATE TABLE `outstanding_bills` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `amount` double NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  `order_id` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outstanding_bills`
--

INSERT INTO `outstanding_bills` (`id`, `user_id`, `description`, `amount`, `status`, `order_id`, `created_at`) VALUES
(2, 12, 'hi', 300, 1, '55194032876', '2019-01-09 09:38:06'),
(3, 21, 'Outstanding', 123, 1, '55240798136', '2019-01-16 14:08:57'),
(6, 21, 'Outstanding', 10, 1, '55937860214', '2019-01-16 15:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `payment_id` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `transaction_id` varchar(100) DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `amount_paid` double DEFAULT NULL,
  `narration` text,
  `payment_description` text,
  `meter_no` varchar(100) DEFAULT NULL,
  `token_no` varchar(255) DEFAULT NULL,
  `token_desc` varchar(255) DEFAULT NULL,
  `token_amount` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `payment_type` varchar(50) NOT NULL DEFAULT 'Card',
  `coupon_id` int(11) DEFAULT NULL,
  `vend_log` longtext,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payment_id`, `user_id`, `amount`, `quantity`, `transaction_id`, `status`, `bank`, `amount_paid`, `narration`, `payment_description`, `meter_no`, `token_no`, `token_desc`, `token_amount`, `type`, `payment_type`, `coupon_id`, `vend_log`, `date_created`) VALUES
(1, '55300281764', 18, 1000, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '43645747', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-09 19:56:59'),
(4, '55821743096', 19, 2000, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '2534636', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-09 20:56:53'),
(14, '55243769018', 19, 2000, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '2534636', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-10 08:53:25'),
(15, '55109743826', 19, 2000, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '2534636', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-10 08:54:45'),
(16, '55612009734', 21, 1, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 12:53:10'),
(17, '55703296148', 21, 1, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 12:56:33'),
(18, '55871926430', 21, 1, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 12:58:42'),
(19, '55608927431', 21, 1, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 13:18:28'),
(20, '55184620073', 21, 1, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 13:22:23'),
(21, '55130768924', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 13:29:12'),
(22, '55781096432', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'SERVICE PAYMENTS', 'Card', NULL, NULL, '2019-01-14 14:20:49'),
(23, '55412730869', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 14:27:14'),
(24, '55394276810', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 15:11:48'),
(25, '55100892436', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 15:51:28'),
(26, '55163087429', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 15:56:16'),
(27, '55412960087', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 16:05:02'),
(28, '55760913842', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 16:05:47'),
(29, '55341972800', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'SERVICE PAYMENTS', 'Card', NULL, NULL, '2019-01-14 20:04:55'),
(30, '55196072438', 21, 5, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 20:09:02'),
(31, '55736421009', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 20:13:53'),
(32, '55006971483', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 20:23:22'),
(33, '55746209813', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 20:40:23'),
(34, '55237490681', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 20:47:37'),
(35, '55729461083', 21, 10, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 20:58:01'),
(36, '55649130287', 21, 10, 1, 'QR5c3cf8bd067f8', 'Paid', 'QR', 10, NULL, 'Transaction Successful', '292345742', NULL, NULL, NULL, 'Service', 'Card', NULL, NULL, '2019-01-14 21:03:25'),
(37, '55240798136', 21, 123, 1, 'NPTEST190116021307000000', 'Paid', 'MPGS', 124, NULL, 'Approved', '292345742', NULL, NULL, NULL, 'Water Pump', 'Card', NULL, NULL, '2019-01-16 14:13:14'),
(38, '55614309872', 21, 30, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Security Charge', 'Card', NULL, NULL, '2019-01-16 14:18:55'),
(39, '55261908734', 21, 30, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Security Charge', 'Card', NULL, NULL, '2019-01-16 14:32:54'),
(40, '55978264100', 21, 890, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'BILL', 'Card', NULL, NULL, '2019-01-16 14:55:59'),
(41, '55143608279', 21, 2000, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'PRODUCT', 'Card', NULL, NULL, '2019-01-16 15:03:37'),
(42, '55831240076', 21, 2000, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Water Pump', 'Card', NULL, NULL, '2019-01-16 15:06:04'),
(43, '55937860214', 21, 10, 1, 'NPTEST190116031912000000', 'Paid', 'MPGS', 110, NULL, 'Approved', '292345742', NULL, NULL, NULL, 'BILL', 'Card', NULL, NULL, '2019-01-16 15:19:17'),
(44, '55823149600', 21, 2000, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Water Pump', 'Card', NULL, NULL, '2019-01-16 15:19:35'),
(45, '55413289607', 21, 2000, 1, NULL, 'Pending', NULL, NULL, NULL, NULL, '292345742', NULL, NULL, NULL, 'Water Pump', 'Card', NULL, NULL, '2019-01-16 15:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `take_or_pay` varchar(255) NOT NULL,
  `base_charge` varchar(255) NOT NULL,
  `account_number` text NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `property_type` varchar(255) NOT NULL,
  `product_type` varchar(255) NOT NULL,
  `partner_type` varchar(255) NOT NULL,
  `product_status` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_price`, `take_or_pay`, `base_charge`, `account_number`, `product_image`, `property_type`, `product_type`, `partner_type`, `product_status`, `created`) VALUES
(1, 'December Power', '2000', '', '', '0060799442', '', 'Terrace', 'Power', 'Partner', 0, '2019-01-09 18:59:03'),
(3, 'December Water Bill', '3000', '', '', '0060799442', '', 'Terrace', 'Water Pump', 'Partner', 0, '2019-01-09 19:22:02'),
(4, 'December Security Charge', '1000', '', '', '0060799442', '', 'Terrace', 'Security Charge', 'Partner', 0, '2019-01-09 19:22:21'),
(5, 'December Service', '1', '', '', '0060799442', '', 'Terrace', 'Service', 'Partner', 0, '2019-01-14 12:51:47'),
(6, 'December Service', '10', '', '', '0060799442', '', 'Detached', 'Service', 'Partner', 0, '2019-01-14 20:12:13'),
(7, 'December Water Bill', '2000', '', '', '0060799442', '', 'Detached', 'Water Pump', 'Partner', 0, '2019-01-09 21:23:03'),
(8, 'December Security Charge', '1500', '', '', '0060799442', '', 'Detached', 'Security Charge', 'Partner', 0, '2019-01-09 21:23:32');

-- --------------------------------------------------------

--
-- Table structure for table `service_charges`
--

CREATE TABLE `service_charges` (
  `id` int(11) NOT NULL,
  `meter_no` varchar(250) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `amount_due` int(11) DEFAULT NULL,
  `total_paid` int(11) DEFAULT '0',
  `advance_payment` int(11) NOT NULL DEFAULT '0',
  `advance_month` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_charges`
--

INSERT INTO `service_charges` (`id`, `meter_no`, `user_id`, `product_id`, `amount_due`, `total_paid`, `advance_payment`, `advance_month`, `date_created`) VALUES
(1, '43645747', 18, 5, 0, 1000, 0, 0, '2019-01-09 20:56:29'),
(2, '253463689', 12, 6, 0, 4000, 2000, 1, '2019-01-10 09:53:03'),
(3, '292345742', 21, 6, 0, 10, 0, 0, '2019-01-10 12:17:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `name`, `password`, `email`, `mobile_no`) VALUES
(1, 15, 'first second', 'da0fdc4ba72117e4495e1a9df336b957a5de7f641d5abf2c428b0f1d1e55ee8a', 'manie94@gmail.com', 99847384);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_group_to_group`
--
ALTER TABLE `aauth_group_to_group`
  ADD PRIMARY KEY (`group_id`,`subgroup_id`);

--
-- Indexes for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perm_to_group`
--
ALTER TABLE `aauth_perm_to_group`
  ADD PRIMARY KEY (`perm_id`,`group_id`);

--
-- Indexes for table `aauth_perm_to_user`
--
ALTER TABLE `aauth_perm_to_user`
  ADD PRIMARY KEY (`perm_id`,`user_id`);

--
-- Indexes for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `full_index` (`id`,`sender_id`,`receiver_id`,`date_read`);

--
-- Indexes for table `aauth_users`
--
ALTER TABLE `aauth_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_user_to_group`
--
ALTER TABLE `aauth_user_to_group`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_index` (`user_id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `house_address`
--
ALTER TABLE `house_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meter_tariff`
--
ALTER TABLE `meter_tariff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outstanding_bills`
--
ALTER TABLE `outstanding_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupon_id` (`coupon_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_charges`
--
ALTER TABLE `service_charges`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `meter_no` (`meter_no`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aauth_users`
--
ALTER TABLE `aauth_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `house_address`
--
ALTER TABLE `house_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `meter_tariff`
--
ALTER TABLE `meter_tariff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outstanding_bills`
--
ALTER TABLE `outstanding_bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `service_charges`
--
ALTER TABLE `service_charges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
